# Developer Documentation and Environment Setup
This repository holds the ADRs for the MoonCat​Rescue project, as well as scripts and tools to help implement those working standards. Browse through the documents in the `adr` folder to learn about coding standards for these projects.

In general, getting started developing on a project within the MoonCat​Rescue ecosystem requires:

- Install [Docker](https://www.docker.com/)
- Run `docker build -t node:mooncat -f Dockerfile.node .` to create the `node:mooncat` image locally

Then, you can check out any other repository in this group, and run `docker-compose up` to get a development environment running. For more details, see [ADR0002](./adr/0002-docker-development-environments.md), or the `README` of the individual project repositories.

# ETH Security Toolbox environment
The Trail of Bits team has put together an [Ethereum Security Toolbox](https://github.com/trailofbits/eth-security-toolbox) that is a Docker image with several smart contract analysis tools. In order to work with the local user setup the MoonCat​Rescue team uses (see [ADR0002](./adr/0002-docker-development-environments.md)), the `ethsec` user the Trail of Bits team uses in their image needs to be switched over to UID 25600. To make a local image that has that, run:

```sh
docker pull trailofbits/eth-security-toolbox
docker build -t eth-security-toolbox:mooncat -f Dockerfile.security-toolbox .
```
