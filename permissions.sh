#!/usr/bin/env bash

chown mooncat:mooncat -R .                        # Make everything owned by the mooncat user
chmod g+w -R .                                    # Allow all users in the mooncat group to edit
find -type d ! -perm -g+s -exec chmod g+s {} \;   # Find all folders that don't have the 'group' sticky bit set and set it
find -type d ! -perm -o+rx -exec chmod o+rx {} \; # Find all folders that aren't readable by 'others' and make them readable
