# 0000 - ADR Template
**Updated:** <!-- DD Mon YYYY -->

## Status
<!-- Proposed | Accepted | Rejected | Deprecated | Superseded -->

## Context
<!-- What is the issue that we're seeing, that is motivating this decision or change -->

## Decision
<!-- What is the change that we're actually proposing or doing. -->

## Consequences
<!-- Outcomes, both positive and negative -->
