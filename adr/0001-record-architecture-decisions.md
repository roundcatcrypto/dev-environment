# 0001 - Record Architecture Decisions
**Updated:** 1 Sep 2022

## Status
<!-- Proposed | Accepted | Rejected | Deprecated | Superseded -->
Accepted

## Context
<!-- What is the issue that we're seeing, that is motivating this decision or change -->
We need to record the architecture deciions made for this team and company.

## Decision
<!-- What is the change that we're actually proposing or doing. -->
We will use Architecture Decision Records, as described by [this repository](https://github.com/joelparkerhenderson/architecture-decision-record).

ADRs will be written in Markdown and stored in the `dev-environment` repository (version-controlled by Git, stored in GitLab). They will be rendered into HTML on the GitLab site for browsing and review.

## Consequences
<!-- Outcomes, both positive and negative -->
See blog article linked under "Decision".

To create a new ADR, create a new file in this folder, copying the `0000-TEMPLATE.md` file for structure.
