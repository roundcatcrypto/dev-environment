# 0004 - Public Testnet
**Updated:** 1 Sep 2022

## Status
<!-- Proposed | Accepted | Rejected | Deprecated | Superseded -->
Accepted

## Context
<!-- What is the issue that we're seeing, that is motivating this decision or change -->
Most of the contract testing done for MoonCat​Rescue projects is done using [Hardhat](https://hardhat.org/) on locally-running, short-lived, personal testnets. However, in order to see how our contracts interact with other projects (especially the web front-ends of other projects), deploying them to one of the long-running, [public Ethereum testnets](https://ethereum.org/en/developers/docs/networks/#testnets) can be helpful.

Görli is a long-lived, public testnet that could be used. However, re-deploying the same or similar contracts over and over again wouldn't be effective. And using the testnet at all requires "test Ether", which doesn't just appear in your wallet like it does with Hardhat.

## Decision
<!-- What is the change that we're actually proposing or doing. -->
Use the [Görli](https://ethereum.org/en/developers/docs/networks/#goerli) (a.k.a. Goerli) testnet when needing to do public testing or integration testing with other applications.

### Shared development addresses
To keep organized on Görli as a team, the project maintainers use a shared mnemonic phrase to keep testnet ETH in, so those developers don't need to each get their own cache of testnet ETH and other testnet assets. These are the first few accounts from that seed phrase; each are given a nickname/persona to make referring to them easier:

1. Alice: [`0x0EDB5F5BDE6269cAe419c711B8E40962681B62e5`](https://goerli.etherscan.io/address/0x0EDB5F5BDE6269cAe419c711B8E40962681B62e5)
2. Bob: [`0xED81627B4c9f4c3061299C7347d29420770a558A`](https://goerli.etherscan.io/address/0xED81627B4c9f4c3061299C7347d29420770a558A)
3. Carol: [`0x14537966E61B1fE99EEE866777C55465Db679FBB`](https://goerli.etherscan.io/address/0x14537966E61B1fE99EEE866777C55465Db679FBB)
4. Deb: [`0x93f5D57DbE5E3811c37273368704c080Ebd60a25`](https://goerli.etherscan.io/address/0x93f5D57DbE5E3811c37273368704c080Ebd60a25)
5. Eve: [`0x40255A452566d929F84e89E97e078065C0de142b`](https://goerli.etherscan.io/address/0x40255A452566d929F84e89E97e078065C0de142b)

Following the conventions of [Alice and Bob as cryptographic character stand-ins](https://en.wikipedia.org/wiki/Alice_and_Bob), "Eve" is cast as the eavesdroppper/evil character, so is the address that by default should have no special privileges, but then gets used to try and break the system (attempting actions she shouldn't be allowed to do).

As "Alice" and "Bob" should emulate end-users, they should not be set as default administrators of different contracts. Instead, "Deb" is the account that should be set as Administrator/Creator/Deployer of contracts on Görli.

### Faucets
[Goerli PoW Faucet](https://goerli-faucet.pk910.de/) uses CPU mining as the compensation for the test ETH. Set it up to hash for a while, and then stop and claim when ready.

The **Alchemy**-supported faucet at https://goerlifaucet.com// will give 0.05 testnet ETH/day, if you sign in with an Alchemy account.

**Chainlink** hosts a faucet at https://faucets.chain.link/goerli, which will give out 0.1 testnet ETH repeatedly, but you need to complete a Google Captcha for each push.

**Paradigm** hosts a "multi-faucet" at https://faucet.paradigm.xyz/, which you need to sign into Twitter to be able to use, and it gives test ETH, [test wETH](https://goerli.etherscan.io/address/0xb4fbf271143f4fbf7b91a5ded31805e42b2208d6), and [fake NFTs](https://goerli.etherscan.io/address/0xf5de760f2e916647fd766b4ad9e85ff943ce3a2b) across all testnets at once.

### Third-party Contracts

- Etherscan: [blockchain explorer](https://goerli.etherscan.io/)
- Opensea: [web UI](https://testnets.opensea.io/)
- Ethereum Name Service (ENS): The "Deb" account has registered the `mooncatrescue.eth` ENS name on Goerli (expires 1 Jun 2025), which can be used to set subdomains to make easy pointers to things on that network, if desired. [ENS manager](https://app.ens.domains/)

<!--- Arbitrum on Rinkeby: [guide](https://ieigen.medium.com/arbitrum-testnet-guide-5edaab31171b), [documentation](https://developer.offchainlabs.com/docs/public_testnet), [blockchain explorer](https://rinkeby-explorer.arbitrum.io/), [bridge](https://bridge.arbitrum.io/)-->

## MoonCat​Rescue deployments

**Gnosis Safe:** [`0x723b42F4e1252Defb6B8990df813E3c64C0F102C`](https://gnosis-safe.io/app/gor:0x723b42F4e1252Defb6B8990df813E3c64C0F102C/home)
Set up to be a 2-of-4 multisig, using the first four addresses from the development seed phrase.

**Chimera MoonCats:** [`0x8f19d82014444051688B5886FA28b0E902B50c43`](https://goerli.etherscan.io/address/0x8f19d82014444051688b5886fa28b0e902b50c43)
Basic ERC721 implementation; does not implement any of the ERC998 functionality.

**JumpPort:** [`0x90A89601FF085e03B3584Eb784ff02359FC50432`](https://goerli.etherscan.io/address/0x90a89601ff085e03b3584eb784ff02359fc50432)

**MoonCatReference:** [`0x8dc4265Dbe3b2C446Ef21ff051AF918F116f1529`](https://goerli.etherscan.io/address/0x8dc4265Dbe3b2C446Ef21ff051AF918F116f1529)

**MoonCatRescueTraits:** [`0x6FC1D50A72533BEaca8a5D741Dc9f48808260a03`](https://goerli.etherscan.io/address/0x6FC1D50A72533BEaca8a5D741Dc9f48808260a03)

**LootprintTraits:** [`0x7F994F20a5C894a96CA7185640Ed1fC2c9c3AfAa`](https://goerli.etherscan.io/address/0x7F994F20a5C894a96CA7185640Ed1fC2c9c3AfAa)

## Consequences
<!-- Outcomes, both positive and negative -->
The Görli network is one of the more widely-used testnets, and target applications MoonCat​Rescue wants to interact with are already present there, which makes it a good choice to use. If those other applications move to other testnets (possibly post-Merge?), MoonCat​Rescue may need to reconsider and move its infrastructure to another testnet.
